#define NOMINMAX 
#include "network.h"
#include <thread>     
#include "sprite/spriteImpl.h" 
#include "sprite/UI/widget.h"
#include "shader.h"





void
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type, severity, message);
}

template<class T>
void render(sprite<T>* data, shader s)
{
	if (data->update)
	{
		data->updateBuffer();
	}


	GLenum vertexType = [&]() {
		if (std::is_same<T, u16>::value) { return GL_UNSIGNED_SHORT; }
		else if (std::is_same<T, f32>::value) { return GL_FLOAT; }
		else { return 0; }
	}();
	glBindBuffer(GL_ARRAY_BUFFER, data->vertexBuffer.buffer);
	glBindTexture(GL_TEXTURE_2D, data->tex->buffer.buffer);

	glEnableVertexAttribArray(s.vertexAttribPos);
	glVertexAttribPointer(
		s.vertexAttribPos,
		2,
		vertexType,
		GL_FALSE,
		sizeof(vertex<T>),
		(void*)0
	);

	glEnableVertexAttribArray(s.UVAttribPos);
	glVertexAttribPointer(
		s.UVAttribPos,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(vertex<T>),
		(void*)(2 * sizeof(T))
	); 

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data->elementBuffer.buffer);


	glDrawElements(
		GL_TRIANGLES,      // mode
		(data->vertices.size() / 4) * 6,    // count
		GL_UNSIGNED_SHORT,   // type
		(void*)0           // element array buffer offset
	);
}

void renderThings(windowState& state)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(state.shaders.spriteShader.program);
	glUniformMatrix4fv(glGetUniformLocation(state.shaders.spriteShader.program, "viewMatrix"), 1, GL_FALSE, state.camera.data());

	//map[map.currentRoom].iterate
	room& currentRoom = state.map->operator[](state.map->currentRoom);
	render(&currentRoom, state.shaders.spriteShader);

	for (size_t i = 0; i < currentRoom.objects.size(); i++)
	{
		render(&currentRoom.objects[i], state.shaders.spriteShader);
	}


	for (size_t i = 0; i < state.enemies[state.map->currentRoom].size(); i++)
	{
		enemy & m = state.enemies[state.map->currentRoom][i];
 
		for (size_t j = 0; j < m.weapons.size(); j++)
		{
			weapon& w = m.weapons[j];
			w.iterateprojectiles(state.players, state.map);
			for (size_t k = 0; k < w.projectiles.size(); k++)
			{
				render(&w.projectiles[k], state.shaders.spriteShader);
			}
		}

		for (size_t j = 0; j < m.data.size(); j++)
		{
			render(&m.data[j], state.shaders.spriteShader);
		}

	}


	for (size_t i = 0; i < state.players.size(); i++)
	{
		player & p = state.players[i]; 

		for (size_t j = 0; j < p.weapons.size(); j++)
		{
			weapon& w = p.weapons[j];
			w.iterateprojectiles(state.enemies[state.map->currentRoom], state.map);
			for (size_t k = 0; k < w.projectiles.size(); k++)
			{
				render(&w.projectiles[k], state.shaders.spriteShader);
			}
		}

		for (size_t j = 0; j < p.data.size(); j++)
		{
			render(&p.data[j], state.shaders.spriteShader);
		}

	}
	glUseProgram(state.shaders.minimapShader.program);
	glUniform2f(glGetUniformLocation(state.shaders.minimapShader.program, "pos"), state.width - currentRoom.width, state.height - currentRoom.height);

	render(&currentRoom, state.shaders.minimapShader);
	//render(&state.players[0].controlPackage.minimapIndicator, state.shaders.minimapShader);

	GLWMSwapBuffers(state.context);
}

void runGameLoopSingleplayer(windowState& state, windowHandle handle)
{ 
	timer t;
	while (true)
	{ 
		t.start();

		

		room& currentRoom = state.map->operator[](state.map->currentRoom); 

		for (size_t i = 0; i < state.enemies[state.map->currentRoom].size(); i++)
		{
			enemy & m = state.enemies[state.map->currentRoom][i];

			if (m.health == 0)
			{
				state.enemies[state.map->currentRoom].erase(state.enemies[state.map->currentRoom].begin() + i);
				continue;
			}

			m.moveEntity(state.map, state.enemies[state.map->currentRoom]); 

		}

		for (size_t i = 0; i < state.players.size(); i++)
		{
			player & p = state.players[i];

			vector2D<f32> initialPos = p.data[0].getPos();
			p.moveEntity(state.map, state.players); 
			state.camera[12] += (128.0f / state.width) * (initialPos.x - p.data[0].vertices[0].x);
			state.camera[13] -= (128.0f / state.height) * (initialPos.y - p.data[0].vertices[0].y); 
			if (state.players[0].controlPackage.controlManager.leftButtonDown)
			{
				p.shoot(vector2D<f32>{static_cast<f32>(state.width / 2.0f), static_cast<f32>(state.height / 2.0f)});
			}
			//bar.update(p.health);
			if (p.health == 0)
			{
				exit(1);
			} 

		}




		/*glUseProgram(shaders.UIShader.program);
		for (size_t i = 0; i < bar.data.size(); i++)
		{

			render(&bar.data[i], shaders.UIShader);
		}*/

		
		GLWMPollEvents(handle);
		renderThings(state);
		//std::cout << (ms(16) - t.getTimeElapsed<ms>()).count() << std::endl;
		std::this_thread::sleep_for(ms(1000 / globals::fps) - t.getTimeElapsed<ms>());
		globals::currentTick = ((globals::currentTick) % globals::fps) + 1;
	}

}

void runGameLoopMultiplayerHost(windowState& state, windowHandle handle)
{
	timer t;

	
	while (true)
	{
		t.start();



		room& currentRoom = state.map->operator[](state.map->currentRoom);

		for (size_t i = 0; i < state.enemies[state.map->currentRoom].size(); i++)
		{
			enemy & m = state.enemies[state.map->currentRoom][i];

			if (m.health == 0)
			{
				state.enemies[state.map->currentRoom].erase(state.enemies[state.map->currentRoom].begin() + i);
				continue;
			}

			m.moveEntity(state.map, state.enemies[state.map->currentRoom]);

		}

		for (size_t i = 0; i < state.players.size(); i++)
		{
			player & p = state.players[i];

			vector2D<f32> initialPos = p.data[0].getPos();
			p.moveEntity(state.map, state.players);
			state.camera[12] += (128.0f / state.width) * (initialPos.x - p.data[0].vertices[0].x);
			state.camera[13] -= (128.0f / state.height) * (initialPos.y - p.data[0].vertices[0].y);
			if (state.players[0].controlPackage.controlManager.leftButtonDown)
			{
				p.shoot(vector2D<f32>{static_cast<f32>(state.width / 2.0f), static_cast<f32>(state.height / 2.0f)});
			}
			//bar.update(p.health);
			if (p.health == 0)
			{
				exit(1);
			}

		}




		/*glUseProgram(shaders.UIShader.program);
		for (size_t i = 0; i < bar.data.size(); i++)
		{

			render(&bar.data[i], shaders.UIShader);
		}*/


		GLWMPollEvents(handle);
		renderThings(state);
		//std::cout << (ms(16) - t.getTimeElapsed<ms>()).count() << std::endl;
		std::this_thread::sleep_for(ms(1000 / globals::fps) - t.getTimeElapsed<ms>());
		globals::currentTick = ((globals::currentTick) % globals::fps) + 1;
	}
}

void runGameLoopMultiplayerClient(windowState& state, windowHandle handle)
{

	timer t;
	while (true)
	{
		t.start();



		room& currentRoom = state.map->operator[](state.map->currentRoom);

		for (size_t i = 0; i < state.enemies[state.map->currentRoom].size(); i++)
		{
			enemy & m = state.enemies[state.map->currentRoom][i];

			if (m.health == 0)
			{
				state.enemies[state.map->currentRoom].erase(state.enemies[state.map->currentRoom].begin() + i);
				continue;
			}

			m.moveEntity(state.map, state.enemies[state.map->currentRoom]);

		}

		for (size_t i = 0; i < state.players.size(); i++)
		{
			player & p = state.players[i];

			vector2D<f32> initialPos = p.data[0].getPos();
			p.moveEntity(state.map, state.players);
			state.camera[12] += (128.0f / state.width) * (initialPos.x - p.data[0].vertices[0].x);
			state.camera[13] -= (128.0f / state.height) * (initialPos.y - p.data[0].vertices[0].y);
			if (state.players[0].controlPackage.controlManager.leftButtonDown)
			{
				p.shoot(vector2D<f32>{static_cast<f32>(state.width / 2.0f), static_cast<f32>(state.height / 2.0f)});
			}
			//bar.update(p.health);
			if (p.health == 0)
			{
				exit(1);
			}

		}




		/*glUseProgram(shaders.UIShader.program);
		for (size_t i = 0; i < bar.data.size(); i++)
		{

			render(&bar.data[i], shaders.UIShader);
		}*/


		GLWMPollEvents(handle);
		renderThings(state);
		//std::cout << (ms(16) - t.getTimeElapsed<ms>()).count() << std::endl;
		std::this_thread::sleep_for(ms(1000 / globals::fps) - t.getTimeElapsed<ms>());
		globals::currentTick = ((globals::currentTick) % globals::fps) + 1;
	}
}

void initGame(windowState& state)
{
	roomData constructData;

	dungeon* map = new dungeon(10, 1, constructData);
	state.map = map;
	state.players.reserve(2);
	state.players.emplace_back(player(vector2D<f32>(map->playerSpawn), 0));

	timer t;

	for (size_t i = 0; i < map->rooms.size(); i++)
	{
		state.enemies.emplace_back(std::vector<enemy>());
		for (size_t j = 0; j < map->rooms[i].spawnPoints.size(); j++)
		{
			vector2D<u16> pos = map->rooms[i].spawnPoints[j];
			state.enemies[i].emplace_back(enemy(vector2D<f32>(pos), 1));
		}
	}

	state.camera[12] -= (64.0f / state.width) * (2 * map->playerSpawn.x - state.width / 64.0f - 32.0f / state.width);
	state.camera[13] += (64.0f / state.height) * (2 * map->playerSpawn.y - state.height / 64.0f - 32.0f / state.height);

}



void shareGame(windowState& state, networkSocket& socket)
{
	std::string msg = "dungeon(" + std::to_string(state.map->rooms.size()) + ");\n";
	sendInfo(socket, msg);
	 
	std::vector<char> buf;
	for(size_t i  = 0; i < state.map->rooms.size(); i++)
	{  
		msg = "room(" + std::to_string(i) + "," +  std::to_string(state.map->rooms[i].width) + "," + std::to_string(state.map->rooms[i].width) + ",";
		sendInfo(socket, msg);

		for (size_t j = 0; j < state.map->rooms[i].map.size(); j++)
		{
			if (buf.size() == 128)
			{
				sendInfo(socket, buf);
				buf.clear();
			}

			buf.push_back(state.map->rooms[i].map[j]);
		}
		sendInfo(socket, buf);
		buf.clear(); 
		sendInfo(socket, std::string(");\n"));
	} 

	msg = "spawnPoint(" + std::to_string(state.map->playerSpawn.x) + "," + std::to_string(state.map->playerSpawn.y) + ");\n";
	sendInfo(socket, msg);
}

std::string findSubstring(std::string& in, size_t pos)
{
	size_t index = in.find("=");

	size_t numRooms = 0;
	if (index != std::string::npos)
	{
		size_t index2 = in.find(";");
		if (index2 != std::string::npos)
		{
			return in.substr(index + 1, index2 - index);
		}
	}
	return "";
}

std::string getFromVector(std::vector<char> in)
{
	return std::string(in.begin(), in.end());
}

std::pair<std::string, std::vector<f32>> interpretMessage(std::string msg, u8 type = 0)
{ 
	size_t findPos = 0; 
	size_t findPos2 = 0;
	 
	findPos = msg.find('(', findPos);
	std::string msgName = msg.substr(0, findPos); 
	std::vector<f32> args;
	
	 
	size_t endPos = msg.find(')', findPos);
	findPos += 1;
	while (msg.find(',', findPos) < endPos)
	{
		findPos2 = msg.find(',', findPos );
		args.push_back(atof(msg.substr(findPos , findPos2 - findPos).c_str()));
		findPos = findPos2 + 1;
	}  

	findPos2 = msg.find(')', findPos);
	if (type == 0)
	{
		args.push_back(atof(msg.substr(findPos, findPos2 - findPos).c_str()));
		return { msgName, args };
	} 

	for (int i = findPos; i < findPos2; i++)
	{ 
		args.push_back(static_cast<f32>(msg[i]));
	}

	return { msgName, args };
}

void recieveGame(windowState& state, networkSocket& socket)
{


	size_t bufferSize; 

	std::pair<std::string, std::vector<f32>> message = interpretMessage(readInfo(socket, bufferSize));

	size_t numRooms = 0; 

	bool isCompleted = false; 
	 
	numRooms = message.second[0]; 

	std::vector<std::vector<u8>> mapData;
	std::vector<u16> widths;
	std::vector<u16> heights;
	
	size_t currentRoom = 0;
	for (size_t i = 0; i < numRooms; i++)
	{
		message = interpretMessage(readInfo(socket, bufferSize), 1);  
		currentRoom = message.second[0];
		widths.push_back(message.second[1]);
		heights.push_back(message.second[2]);
		mapData.push_back(std::vector<u8>(message.second.begin() + 3, message.second.end()));
	}


	message = interpretMessage(readInfo(socket, bufferSize));
 
	u16 playerSpawnX = message.second[0];
	u16 playerSpawnY = message.second[1];
	dungeon* map = new dungeon(mapData, widths, heights);

	map->playerSpawn = vector2D<u16>{ playerSpawnX, playerSpawnY }	;
	state.map = map;
	state.camera[12] -= (64.0f / state.width) * (2 * map->playerSpawn.x - state.width / 64.0f - 32.0f / state.width);
	state.camera[13] += (64.0f / state.height) * (2 * map->playerSpawn.y - state.height / 64.0f - 32.0f / state.height);



}


#include <iostream>
int main(void) 
{
	char* h = new char[5];
	h[0] = 3;
	h[1] = 2;
	h[2] = 0;
	h[3] = 3;
	
	srand(time(NULL));
	globals::fps = 60;
	globals::speedModifier = 60 / globals::fps;
	globals::currentTick = 0;

	HWND handle = initGLWindow();
	HDC context = getContext(handle);

	GLWMPollEvents(handle);
	windowState& state = getState(handle);
	state.context = context;
	textureManager::init();
	shaderManager shaders(state.width, state.height);
	state.shaders = shaders;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	 
	
	 
	
	UIContainer* container = new UIContainer(5, 5, 800, 600, { 0,0 });
	//progressBar bar({ 0,0 }, 25, 100, 25, container, alignment::topLeft);
	 
	printf("\n\n1 for singleplayer, 2 for host, 3 for client");
	int gameType;
	std::cin >> gameType;
	if (gameType == 1)
	{
		while (true)
		{
			runGameLoopSingleplayer(state, handle);
		}
	}
	if (gameType == 2)
	{
		
		asio::io_context io_context;
		asio::ip::tcp::acceptor acceptor(io_context, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), 13));

		asio::ip::tcp::socket socket(io_context);
		try 
		{
			acceptor.accept(socket);
		}
		catch (std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}  
		initGame(state); 
		shareGame(state, socket);

		while (true)
		{
			runGameLoopMultiplayerHost(state, handle);
		}
	}
	if (gameType == 3)
	{
		
		asio::io_context io_context; 
		asio::ip::tcp::socket socket(io_context);
		asio::ip::tcp::resolver resolver(io_context);

		asio::ip::tcp::resolver::results_type endpoints =
		resolver.resolve("localhost", "daytime");
		
		bool isConnected = false;
		while(isConnected == false)
		{ 
			try
			{ 
				asio::connect(socket, endpoints);
				isConnected = true;
			}
			catch (std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}
		} 
		 
		recieveGame(state, socket);
		state.players.reserve(2);
		state.players.emplace_back(player(vector2D<f32>(state.map->playerSpawn), 0));

		timer t;

		for (size_t i = 0; i < state.map->rooms.size(); i++)
		{
			state.enemies.emplace_back(std::vector<enemy>());
			for (size_t j = 0; j < state.map->rooms[i].spawnPoints.size(); j++)
			{
				vector2D<u16> pos = state.map->rooms[i].spawnPoints[j];
				state.enemies[i].emplace_back(enemy(vector2D<f32>(pos), 1));
			}
		}
		while (true)
		{
			runGameLoopMultiplayerClient(state, handle);
		}
	}
	return 0;
}