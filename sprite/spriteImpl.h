#ifndef SPRITEIMPL  
#define SPRITEIMPL

#include "../globals.h"


template <typename T1, typename T2>
bool detectCollision(sprite<T1>& first, sprite<T2>& second)
{
	if ((first.vertices[0].x > second.vertices[0].x && first.vertices[0].x < second.vertices[1].x)
		|| (first.vertices[1].x > second.vertices[0].x  && first.vertices[1].x < second.vertices[1].x))
	{
		if ((first.vertices[0].y > second.vertices[0].y && first.vertices[0].y < second.vertices[3].y)
			|| (first.vertices[3].y > second.vertices[0].y  && first.vertices[3].y < second.vertices[3].y))
		{
			return true;
		};
	}
	return false;
}
template <typename T1, typename T2>
bool detectCollision(sprite<T1>& first, multiSprite<T2>& second)
{
	for (size_t i = 0; i < second.data.size(); i++)
	{
		if (detectCollision(first, second.data[i]))
		{
			return true;
		}
	}
	return false;
}




inline bufferManager::bufferManager(bufferManager&& other)
{
	buffer = std::move(other.buffer);
	other.buffer = 0;
}

inline bufferManager& bufferManager::operator =(bufferManager&& other)
{
	buffer = std::move(other.buffer);
	other.buffer = 0;
	return *this;
}

inline bufferManager::~bufferManager()
{
	glDeleteBuffers(1, &buffer);
}

template <typename T>
inline sprite<T>::sprite(size_t numVertices)
{
	vertices.reserve(numVertices);
	for (size_t i = 0; i < numVertices; i++)
	{
		vertices.emplace_back(vertex<T>{});
	}

	glGenBuffers(1, &vertexBuffer.buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer.buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_DYNAMIC_DRAW);

	std::vector<u16> indices;
	indices.reserve((numVertices / 4) * 6);

	for (size_t i = 0; i < indices.capacity() / 6; i++)
	{
		int currentIndex = i * 4;
		indices.emplace_back(0 + currentIndex);
		indices.emplace_back(1 + currentIndex);
		indices.emplace_back(2 + currentIndex);
		indices.emplace_back(2 + currentIndex);
		indices.emplace_back(3 + currentIndex);
		indices.emplace_back(0 + currentIndex);
	}
	glGenBuffers(1, &elementBuffer.buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer.buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_DYNAMIC_DRAW);

}

template <typename T>
void sprite<T>::setPosVertices(T posLeft, T posTop, T width, T height, size_t vertArrayStart)
{
	// top left corner
	vertices[vertArrayStart].x = posLeft;
	vertices[vertArrayStart].y = posTop;
	// top right
	vertices[vertArrayStart + 1].x = posLeft + width;
	vertices[vertArrayStart + 1].y = posTop;
	// bottom right
	vertices[vertArrayStart + 2].x = posLeft + width;
	vertices[vertArrayStart + 2].y = posTop + height;
	//bottom right  
	vertices[vertArrayStart + 3].x = posLeft;
	vertices[vertArrayStart + 3].y = posTop + height;

	update = true;
}

template <typename T>
void sprite<T>::setTexVertices(f32 posLeft, f32 posTop, f32 texWidth, f32 texHeight, size_t vertArrayStart)
{
	//top left
	vertices[vertArrayStart].u = posLeft;
	vertices[vertArrayStart].v = posTop;
	//top right
	vertices[vertArrayStart + 1].u = posLeft + texWidth;
	vertices[vertArrayStart + 1].v = posTop;
	//bottom right
	vertices[vertArrayStart + 2].u = posLeft + texWidth;
	vertices[vertArrayStart + 2].v = posTop + texHeight;
	//bottom left
	vertices[vertArrayStart + 3].u = posLeft;
	vertices[vertArrayStart + 3].v = posTop + texHeight;

	update = true;
}

template <typename T>
void sprite<T>::rotatePosVertices(f32 theta, size_t vertArrayStart)
{
	i8 orientation = theta < 0 ? -1 : 1;
	f32 s = sin(theta) * orientation;
	f32 c = cos(theta);

	f32 xOffset = vertices[vertArrayStart].x + (getWidth(vertArrayStart / 4) / 2);
	f32 yOffset = vertices[vertArrayStart].y + (getHeight(vertArrayStart / 4) / 2);

	for (size_t index = vertArrayStart; index < vertArrayStart + 4; index++)
	{
		f32 i = vertices[index].x - xOffset;
		f32 j = vertices[index].y - yOffset;

		vertices[index].x = ((i * c) - (j * s)) + xOffset;
		vertices[index].y = ((i * s) + (j * c)) + yOffset;
	}
	update = true;
}

template <typename T>
void sprite<T>::rotateTexVertices(f32 theta, size_t vertArrayStart)
{
	f32 s = sin(theta);
	f32 c = cos(theta);

	f32 xOffset = 0;
	f32 yOffset = 0;

	for (size_t index = vertArrayStart; index < vertArrayStart + 4; index++)
	{
		f32 u = vertices[index].u + xOffset;
		f32 v = vertices[index].v + yOffset;

		vertices[index].u = ((u * c) - (v * s)) + xOffset;
		vertices[index].v = ((u * s) + (v * c)) - yOffset;
	}


	update = true;
}

template <typename T>
void sprite<T>::updateBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer.buffer);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices[0]) * vertices.size(), vertices.data());

	update = false;
}

template <typename T>
void sprite<T>::move(vector2D<T> delta)
{
	for (size_t i = 0; i < vertices.size(); i++)
	{
		vertices[i].x += delta.x;
		vertices[i].y += delta.y;
	}
	update = true;
}

template <typename T>
vector2D<T> sprite<T>::getPos()
{
	return { vertices[0].x, vertices[0].y };
}

template <typename T>
T sprite<T>::getWidth(u8 squareNum)
{
	return static_cast<T>(vertices[(squareNum * 4) + 1].x - vertices[squareNum * 4].x);
}

template <typename T>
T sprite<T>::getHeight(u8 squareNum)
{
	return static_cast<T>(vertices[(squareNum * 4) + 3].y - vertices[squareNum * 4].y);
}

template <typename T>
animatedSprite<T>::animatedSprite(size_t data, std::vector<u8> sequence, u8 ticksBetweenFrames) : sprite<T>(data)
{
	animSequence = sequence;
	ticksPerFrame = ticksBetweenFrames;
}

template <typename T>
void animatedSprite<T>::animate()
{
	texture* tex = this->tex;
	if (globals::currentTick % (ticksPerFrame) == 0)
	{
		currentIndex = (currentIndex + 1) % animSequence.size();
		u8 texturePos = animSequence[currentIndex];
		this->setTexVertices(static_cast<f32>(texturePos % tex->width) / (tex->width), static_cast<f32>(texturePos / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
	}
}

template <typename T>
void animatedSprite<T>::stopAnimation()
{
	texture* tex = this->tex;
	isAnimating = false;
	currentIndex = 0;
	this->setTexVertices(static_cast<f32>(restFrame % tex->width) / (tex->width), static_cast<f32>(restFrame / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
}

 

template <typename T>
entity<T>::entity(vector2D<f32> pos, entityType type)
	: multiSprite({4,4 }, std::vector<std::vector<u8>>{ {0}, { 0 }, { 0 } }, std::vector<u8>{1, 1, 1})
{
	team = type;
	weapons.reserve(3);
	texture* tex;
	switch (type)
	{
	case 0: // player
		// maybe change player weapons to something modular to allow for loading from a save file
		tex = textureManager::getTexture("playerLegs");  
		data[0].setPosVertices(pos.x + 0.1, pos.y + 0.1, 0.8, 0.4, 0);
		data[0].tex = tex;
		data[1].setPosVertices(pos.x + 0.1, pos.y + 0.5, 0.8, 0.4, 0);
		data[1].tex = tex; 
		weapons.emplace_back(weapon(0, reinterpret_cast<entity<controlPackageBase>*>(this)));
		break;
	case 1:

		tex = textureManager::getTexture("gun");
		data[0].tex = tex;
		data[0].setPosVertices(pos.x + 0.1, pos.y + 0.1, 0.8f, 0.8f, 0);
		data[0].setTexVertices(static_cast<f32>(type % tex->width) / (tex->width), static_cast<f32>(type / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
		data[1].tex = tex;
		weapons.emplace_back(weapon(1, reinterpret_cast<entity<controlPackageBase>*>(this)));
		break;
	}
}
template <typename T>
inline entity<T>::entity(entity&& other) : multiSprite<animatedSprite<f32>>(std::move(*this))
{
	team = other.team;
	health = other.health;
	defense = other.defense;
	quadrant = other.quadrant;
	data = std::move(other.data);
	weapons = std::move(other.weapons);
	for (size_t i = 0; i < weapons.size(); i++)
	{
		weapons[i].holder = reinterpret_cast<entity<controlPackageBase>*>(this);
	}
}

template <typename T>
inline entity<T>& entity<T>::operator= (entity&& other)
{
	team = other.team;
	health = other.health;
	defense = other.defense;
	data = std::move(other.data);
	weapons = std::move(other.weapons);
	quadrant = other.quadrant;
	for (size_t i = 0; i < weapons.size(); i++)
	{ 
		weapons[i].holder = reinterpret_cast<entity<controlPackageBase>*>(this);
	}
	return *this;
}

template <typename T>
inline void entity<T>::shoot(vector2D<f32> pos)
{
	vector2D<f32> delta = controlPackage.getFireDelta(pos);
	weapons[currentWeapon].fireprojectile(vector2D<f32>{delta.x / 8 * globals::speedModifier, delta.y/ 8 * globals::speedModifier});
};

template <typename T>
inline void entity<T>::takeDamage(projectile& b)
{
	if (team == reinterpret_cast<entity*>(b.origin->holder)->team)
	{
		return;
	}
	int damageTaken = b.origin->attackPower - defense;

	if (damageTaken > health)
	{
		damageTaken = health;
	}
	health -= damageTaken;
};

u8 getQuadrant(vector2D<f32>);
template <typename T>
inline void entity<T>::moveEntity(dungeon* map, std::vector<entity<T>>& enemies)
{ 
	std::vector<std::vector<u8>> entityAnimSequences =
	{
		{0,1,0,2}, {6,7,8,9}, {11,12,13,14}, {16,17,18,19}, {20,21,20,22}, {26,27,28,29}, {31,32,33,34}, {36,37,38,39}
	};
	std::vector<u8> entityRestPos =
	{
		0, 5, 10, 15, 20, 25, 30, 35
	};
	std::vector<u8> entityAnimTickDiff =
	{
		10,10,10,10,10,10,10,10
	};

	vector2D<f32> delta = controlPackage.getMoveDelta(data[0].getPos(), map, enemies);
	if (delta == vector2D<f32>{ 0, 0 })
	{
		for (size_t i = 0; i < data.size(); i++)
		{
			data[i].stopAnimation();
		}
		return;
	}

	u8 quadrant = getQuadrant(delta);
	if (quadrant != this->quadrant)
	{
		this->quadrant = quadrant;
		for (size_t i = 0; i < data.size(); i++)
		{
			data[i].animSequence = entityAnimSequences[quadrant];
			data[i].restFrame = entityRestPos[quadrant];
			data[i].ticksPerFrame = entityAnimTickDiff[quadrant];
			data[i].currentIndex = 0;
		}
	}

	for (size_t i = 0; i < data.size(); i++)
	{
		data[i].animate();
	}



	if (map->isWall(data[0].getPos() + delta, 0.8, 0.8, map->currentRoom))
	{
		return;
	}

	move({ delta.x / 8 * globals::speedModifier, delta.y / 8 * globals::speedModifier });

};


template<class T>
vector2D<f32> controlManagerPlayer::getMoveDelta(vector2D<f32> pos, dungeon* map, std::vector<entity<T>>& players)
{
	float deltaY = 0;
	float deltaX = 0;
	
	if (controlManager.moveLeft)
	{
		deltaX -= 1;
	}
	if (controlManager.moveRight)
	{
		deltaX += 1;
	}
	if (controlManager.moveUp)
	{
		deltaY -= 1;
	}
	if (controlManager.moveDown)
	{
		deltaY += 1;
	}
	 
	minimapIndicator.setPosVertices(static_cast<u16>(pos.x + (deltaX * 0.1  * globals::speedModifier) - 2), static_cast<u16>(pos.y + (0.1 * deltaY * globals::speedModifier) - 2), 5, 5, 0);
	return { deltaX , deltaY };
}

template<class T>
vector2D<f32> controlManagerEnemy::getMoveDelta(vector2D<f32> enemyPos, dungeon* map, std::vector<entity<T>>& players)
{

	if (globals::currentTick % 59 == 0)
	{
		pathing.clear();
		vector2D<f32> playerPos = players[0].data[0].getPos();
		if (abs(pythagorean(enemyPos, playerPos)) > chaseRange)
		{
			return { 0,0 };
		}
		std::vector<vector2D<u16>> path = map->findPath({ static_cast<u16>(enemyPos.x), static_cast<u16>(enemyPos.y) }, { static_cast<u16>(playerPos.x), static_cast<u16>(playerPos.y) }, 0);
 
		for (size_t i = path.size() - chaseRange; i < path.size() - 1; i++)
		{
			pathing.push_back(path[i]);
		}
		if (pathing.size() < 2) // There's no path to add
		{
			return { 0,0 };
		}
	}
	if (pathing.size() > 0)
	{
		vector2D<f32> pos = enemyPos;
		vector2D<i16> delta = vector2D<i16>{ static_cast<i16>(pathing[pathing.size() - 1].x), static_cast<i16>(pathing[pathing.size() - 1].y) } -vector2D<i16>{static_cast<i16>(pos.x), static_cast<i16>(pos.y)};
		if (delta == vector2D<i16>{0, 0})
		{
			return { 0,0 };
		}
		 

		return vector2D<f32>(delta);


	}
	return { 0,0 };
}

template <typename T>
inline bool projectile::moveprojectile(T& vec, dungeon* map)
{
	switch (origin->damageType)
	{
	case bullet:
		move(delta);
		break;
	case swing:
		// this wont account for direction of swing, how to make it?
		vector2D<f32> holderPos = origin->holder->data[0].getPos();
		move({ holderPos - getPos() });
		break;
	}

	// Detect if the projectile overlaps a collidable object. If so, return true.
	if (map->isWall(getPos(), getWidth(0), getHeight(0), map->currentRoom)) return true;

	for (size_t i = 0; i < vec.size(); i++)
	{
		if (detectCollision(*this, vec[i]))
		{
			vec[i].takeDamage(*this);
			return true;
		}
	}


	return false;
};

#endif  
