#ifndef SPRITE 
#define SPRITE  
#include "../glew.h"
#include "../util.h"
template <typename T>
struct vertex
{
	T x, y;
	f32 u, v;
}; 
   
class bufferManager
{
public:
	u32 buffer = 0;

	bufferManager() = default;
	bufferManager(const bufferManager& other) = delete;
	bufferManager& operator =(bufferManager&) = delete;	
	bufferManager(bufferManager&& other);  
	bufferManager& operator =(bufferManager&& other);
	~bufferManager();
};


class texture
{
public:
	bufferManager buffer;
	u16 width;
	u16 height;
	f32 widthAspect;
	f32 heightAspect;

	texture(std::string, u8, u8);
};

template <typename T>
class sprite
{
public: 
	bufferManager vertexBuffer;
	bufferManager elementBuffer;
	std::vector<vertex<T>> vertices;
	 
	texture* tex; 
	 

	bool update = false;

	sprite(size_t);
	void setPosVertices(T, T, T, T, size_t);
	void setTexVertices(f32, f32, f32, f32, size_t); 
	void rotatePosVertices(f32, size_t); 
	void rotateTexVertices(f32, size_t);
	void move(vector2D<T>);
	void updateBuffer();
	void printVertices()
	{
		for (size_t i = 0; i < vertices.size(); i++)
		{
			printf("%f, %f, %f, %f \n ", vertices[i].x, vertices[i].y, vertices[i].u, vertices[i].v);
		}
		printf("\n");
	};
	vector2D<T> getPos(); 
	T getWidth(u8);
	T getHeight(u8);
}; 

template <typename T>
class animatedSprite : public sprite<T>
{
public:
	std::vector<u8> animSequence;
	bool isAnimating = false;
	u8 ticksPerFrame = 7;
	u8 restFrame = 0;
	i8 currentIndex = 0;

	animatedSprite(size_t, std::vector<u8>, u8);
	void animate();
	void stopAnimation();

};

template<typename ... tailType>
auto getTuple(size_t i, size_t ptr, tailType&&  ... tail)
{
	return std::make_tuple(ptr, tail[i]...);
}

template<class T>
class multiSprite
{
public: 
	template <typename... Args>
	multiSprite(std::vector<size_t> sprData, Args&&... args)
	{
		this->data.reserve(sprData.size());
		for (size_t i = 0; i < sprData.size(); i++)
		{  
			data.emplace_back(std::make_from_tuple<T>(getTuple(i, sprData[i],  args...)));  
		}
	}

	std::vector<T> data;
	  
	void move(vector2D<f32> delta)
	{
		for (size_t i = 0; i < data.size(); i++)
		{
			data[i].move(delta);
		}
	}		
}; 


class textureManager
{
	static std::map<std::string, texture> textures;
public:
	static texture* getTexture(const std::string&); 
	static void init();
};

#endif