#ifndef WIDGET
#define WIDGET
#include "../sprite.h" 


enum alignment
{
	topLeft,
	topMiddle,
	topRight,
	middleLeft,
	middle,
	middleRight,
	bottomLeft,
	bottomMiddle,
	bottomRight
};

class UIContainer;
class widget : public multiSprite<sprite<u16>>
{  
public:
	widget(std::vector<size_t> data, UIContainer* parent) : multiSprite(data), parent(parent) {};
	vector2D<f32> getAdjustedPos(vector2D<f32>, UIContainer*, f32, f32, alignment);
	UIContainer* parent;
};


class UIContainer
{
public:
	UIContainer(u8 numChildren, u8 numElements, u16 width, u16 height, vector2D<f32> setPos, UIContainer* setParent = nullptr)
		: width(width), height(height), pos(setPos), parent(setParent)
	{
		children.reserve(numChildren);
		elements.reserve(numElements);
	}
	u16 width;
	u16 height;
	vector2D<f32> pos;
	UIContainer* parent;

	std::vector<UIContainer*> children;
	std::vector<widget>  elements;
};

class progressBar : public widget
{
public:
	f32* value;
	progressBar(vector2D<f32>, f32, f32, f32, UIContainer*, alignment); 
	void update(f32);
};
#endif  