#include "widget.h"   
#include "../spriteImpl.h"
vector2D<f32> widget::getAdjustedPos(vector2D<f32> offset, UIContainer* parent, f32 width, f32 height, alignment align)
{

	vector2D<f32> posToReturn{ 0,0 };

	offset = vector2D<f32>{ parent->width * (offset.x / 100), parent->height * (offset.y / 100)};
	f32 xAdjustment = 0;
	f32 yAdjustment = 0;

	switch (align)
	{
	case alignment::topLeft:
		posToReturn = vector2D<f32>{ parent->pos.x, parent->pos.y};
		break;
	case alignment::middleLeft:
		posToReturn = vector2D<f32>{ parent->pos.x, parent->pos.y + (parent->height / 3) };
		break;
	case alignment::bottomLeft:
		posToReturn = vector2D<f32>{ parent->pos.x, parent->pos.y + (parent->height / 3 * 2) };
		break;
	case alignment::topMiddle:
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3), parent->pos.y};
		break;
	case alignment::middle:
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3), parent->pos.y + (parent->height / 3)};
		break;
	case alignment::bottomMiddle:
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3), parent->pos.y + (parent->height / 3 * 2) };
		break;
	case alignment::topRight:
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3 * 2), parent->pos.y };
		break;
	case alignment::middleRight: 
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3 * 2), parent->pos.y + (parent->height / 3) };
		break;
	case alignment::bottomRight:
		posToReturn = vector2D<f32>{ parent->pos.x + (parent->width / 3 * 2), parent->pos.y + (parent->height / 3 * 2) };
		break;
	} 

	posToReturn += offset;

	xAdjustment += posToReturn.x < parent->pos.x ? parent->pos.x- posToReturn.x  : 0;
	xAdjustment += posToReturn.x + (parent->width * (width / 100)) > parent->pos.x + parent->width ? -((posToReturn.x + (parent->width * (width / 100)))  - (parent->pos.x + parent->width)) : 0;
	yAdjustment += posToReturn.y < parent->pos.y ? parent->pos.y - posToReturn.y : 0;
	yAdjustment += posToReturn.y + (parent->height * (height/ 100)) > parent->pos.y + parent->height ? -((posToReturn.y + (parent->height * (height / 100))) - (parent->pos.y + parent->height)) : 0;

	posToReturn.x += xAdjustment;
	posToReturn.y += yAdjustment;

	return posToReturn;
}
 


progressBar::progressBar(vector2D<f32> posOffset, f32 val, f32 width, f32 height, UIContainer* parent, alignment align)
	: widget({12}, parent)
{  
	vector2D<f32> pos = getAdjustedPos(posOffset, parent, width, height, align);
	data[0].tex = textureManager::getTexture("healthBar");
	data[0].setPosVertices( pos.x, pos.y, width, height, 0);
	data[0].setTexVertices(0, 0, 0.5f, 1, 0);
	data[0].setPosVertices( pos.x, pos.y, width * (val / 100), height, 4);
	data[0].setTexVertices(0.5f, 0, 0.5f, 1, 4);
}

 

void progressBar::update(f32 val)
{
	val = val > 100.0f ? 100.0f : val;
	val = val < 0.0f ? 0.0f : val;
	
	data[0].setPosVertices( data[0].vertices[0].x, data[0].vertices[0].y , data[0].getWidth(0) * (val / 100), data[0].getHeight(0), 4);
}