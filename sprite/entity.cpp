#include "spriteImpl.h"   
#include <math.h>
 
projectile::projectile(vector2D<f32> pos, vector2D<f32> speed, vector2D<f32> size, weapon* origin, weaponType type) : sprite({4})
{ 
	t.start();
	this->delta = speed;   
	this->origin = origin;
	tex = textureManager::getTexture("bullets");
	setPosVertices(pos.x, pos.y, size.x, size.y, 0);
	setTexVertices(static_cast<f32>(type % tex->width) / (tex->width), static_cast<f32>(type / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
	rotatePosVertices(atan2(speed.y, speed.x), 0);
}

projectile::projectile(vector2D<f32> pos, vector2D<f32> size, weapon* origin, weaponType type) : sprite(4)
{
	t.start();
	this->origin = origin;
	tex = textureManager::getTexture("bullets");
	setPosVertices(pos.x, pos.y, size.x, size.y, 0);
	setTexVertices(static_cast<f32>(type % tex->width) / (tex->width), static_cast<f32>(type / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
}
 



void weapon::fireprojectile(vector2D<f32> delta)
{
	// make this account for different types of weapons
	if (t.getTimeElapsed<ms>() > cooldown && isReloading == false)
	{  
		vector2D<f32> pos = holder->data[0].getPos();
		for (size_t i = 0; i < projectilesPerShot; i++)
		{
			switch(damageType)
			{
			case bullet:
				projectiles.emplace_back(projectile(pos, delta, { 0.35, 0.35 }, this, type));
				break;
			case swing:
				projectiles.emplace_back(projectile(pos, { 0.35, 0.35 }, this, type));
				break;
			}
		}
		t.start();
	}
} 
 


u8 getQuadrant(vector2D<f32> delta)
{
	// i have no idea how to make this better but please tell me

	if (delta.y == -1) //bullet facing north
	{
		if (delta.x == -1)
		{
			return 7; //northwest
		}
		if (delta.x == 0)
		{
			return 0; //north
		}
		if (delta.x == 1)
		{
			return 1; //northeast
		}
	}
	if (delta.y == 0)
	{
		if (delta.x == -1)
		{
			return 6; //west
		}
		if (delta.x == 1)
		{
			return 2; //east
		}
	}
	if (delta.y == 1) //bullet facing south
	{
		if (delta.x == -1)
		{
			return 5; //southwest
		}
		else if (delta.x == 0)
		{
			return 4; //south
		}
		else if (delta.x == 1)
		{
			return 3;//southeast
		}
	}
	printf("fuck\n");
	return 0;
}
  


vector2D<f32> controlManagerPlayer::getFireDelta(vector2D<f32> windowSize)
{ 
	vector2D<f32> delta = vector2D<f32>(controlManager.lastClick) - windowSize;

	float hypotLen = sqrt(pow(delta.x, 2) + pow(delta.y, 2));

	delta.x /= hypotLen;
	delta.y /= hypotLen;
	return delta;
} 

controlManagerPlayer::controlManagerPlayer() : minimapIndicator(4)
{ 
	minimapIndicator.tex = textureManager::getTexture("healthBar");
	minimapIndicator.setTexVertices(0.5, 0, 0.5, 1, 0);
}