#include "spriteImpl.h" 
bool healthKitFunc(player* p)
{
	if (p->health >= 100)
	{
		return false;
	}
	p->health = 100;
	return true;
}

std::vector<mapObjectFunc> mapFuncs
{
	healthKitFunc,
};
std::vector<mapObjectType> funcTypes
{
	walkOverPlayer
};

//make another lookup for width/height sizes
mapObject::mapObject(vector2D<f32> pos, u8 type) : sprite(4)
{
	tex = textureManager::getTexture("mapObjects");
	setPosVertices(pos.x, pos.y, 1, 1, 0); 
	setTexVertices(static_cast<f32>(type % tex->width) / (tex->width), static_cast<f32>(type / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, 0);
	 
	func = mapFuncs[type];
	this->type = funcTypes[type];
}

std::vector<std::vector<trigger>> triggers
{
	{}
};

door::door(vector2D<f32> pos, u8 type) : sprite(4)
{
	tex = textureManager::getTexture("door");
	setPosVertices(pos.x, pos.y, 1, 1, 0);
	setTexVertices(0, 0, 1, 1, 0);
}

bool door::isOpen()
{
	for (size_t i = 0; i < triggers.size(); i++)
	{
		if (!triggers[i]())
		{
			return false;
		}
	}
	return true;
} 
roomConstruct mergeRooms(roomConstruct& first, roomConstruct second, bool& successful )
{  
	size_t index = first.connections[rand() % first.connections.size()];
  
	vector2D<u16> connectionPos = { index % first.width, index / first.width };
	int xPos = connectionPos.x;
	int yPos = connectionPos.y;
	int firstX = 0;
	int firstY = 0;
	u16 newWidth = first.width;
	u16 newHeight = first.height;
	u32 currentRow = 0;

	size_t secondConnection = 0;
	u8 connectionType = [&]() {
		if (index < first.width) return 0; //up  
		else if (first.map[index - first.width] == 0) return 1; //also up 
		else if (index % first.width == 0) return 2; //left   
		else if (first.map[index - 1] == 0) return 3; //also left
		else if ((index + 1) % first.width == 0) return 4; //right 
		else if (first.map[index + 1] == 0) return 5; //also right 
		else if (index + first.width > first.map.size())  return 6;// bottom 
		else if (first.map[index + first.width] == 0) return 7; // also bottom 
		else { return 255; }
	}();

	//this really shouldn't happen but idk
	if (connectionType == 255) 
	{
		first.map[index] = 0; 
		std::vector<size_t> newConnections;
		for (int j = 0; j < first.connections.size(); j++)
		{
			if (first.connections[j] != index)
			{
				newConnections.push_back(first.connections[j]);
			}
		}
		return roomConstruct{ first.map, first.	width, first.height, newConnections };;
	} 

	switch (connectionType)
	{
	case 0: //up, adding new area
		xPos = connectionPos.x - ((second.width - 1) / 2);
		yPos = connectionPos.y - first.height - 1 < 0 ? 0 : connectionPos.y - first.height - 1;
		firstY = second.height - 1; // correct
		firstX = ((second.width / 27) - 1) * 13; // uhhh 
		if (firstX > 0) { newWidth += (second.width - firstX) + (first.width - firstX - second.width); } // correct
		newHeight += second.height - 1; // correct
		secondConnection = (second.height * second.width) - ((second.width) / 2) - 1;  // correct
		break;
	case 1: //up, adding to blank spot on existing vector
		xPos = connectionPos.x - ((second.width - 1) / 2);
		yPos = connectionPos.y - first.height - 1 < 0 ? 0 : connectionPos.y - first.height - 1;
		firstX = ((second.width / 27) - 1) * 13;
		newWidth += (second.width - firstX) + (first.width + firstX - second.width) ; 
		secondConnection = (second.height * second.width) - ((second.width) / 2); 
		break;
	case 2: //left, adding new area 
		yPos = connectionPos.y - ((second.height) / 2);
		firstX = second.width - 1;
		newWidth += second.width - 1;
		secondConnection = ((second.height / 2) * second.width) + second.width - 1; 
		break;
	case 3: //left, adding to blank spot on existing vector 
		yPos = connectionPos.y - ((second.height - 1) / 2);  
		secondConnection = ((second.height / 2) * second.width) + second.width - 1;
		break;
	case 4: //right, adding new area 
		yPos = connectionPos.y - ((second.height - 1) / 2);
		secondConnection = ((second.height / 2) * second.width);
		newWidth += second.width - 1;
		break;
	case 5: //right, adding to blank spot on existing vector 
		yPos = connectionPos.y - ((second.height - 1) / 2);
		secondConnection = ((second.height / 2) * second.width); 
		break;
	case 6:
		xPos = connectionPos.x - ((second.width - 1) / 2);
		secondConnection = second.height * second.width - (second.width / 2) - 1; 
		if (firstX > 0) { newWidth += (second.width - firstX) + (first.width - firstX - second.width); } // correct
		newHeight += second.height - 1;
		break;
	case 7:
		xPos = connectionPos.x - ((second.width - 1) / 2);
		secondConnection = second.height * second.width - (second.width / 2) - 1; 
		if (firstX > 0) { newWidth += (second.width - firstX) + (first.width - firstX - second.width); } // correct
		newHeight += second.height - 1;
		break;
	}
	 
	if (second.map[secondConnection] != 255)
	{
		return roomConstruct{ };
	} 
	 
	std::vector<u8> newRoom(newWidth * newHeight, 0);  
	std::set<size_t> connectionSet;
	size_t i = 0;
	 

	//handle inserting the first room

	for (int y = firstY; y < first.height + firstY; y++)
	{
		for (int x = firstX; x < first.width + firstX; x++)
		{
			int offset = x + (y * newWidth);
			newRoom[offset] = first.map[i];
			i++;
			if (newRoom[offset] == 255)
			{
				connectionSet.insert(offset);
			}
		}
	}    

	i = 0;
	//add the new room
	std::vector<size_t> toRemove;
	for (int y = yPos; y < yPos + second.height; y++)
	{
		for (int x = xPos; x < xPos + second.width; x++)
		{
			int offset = (x + (y * newWidth));
			if (newRoom[offset] == 255)
			{
				newRoom[offset] = 1;
				second.map[i] = 1;
				i++; 
				toRemove.push_back(offset); 
				continue;
			} 
			if (second.map[i] == 255)
			{   
				connectionSet.insert(offset);
			}
			newRoom[offset] = MAX(second.map[i], newRoom[offset]);
			i++;
		}
	} 

	std::vector<size_t> newConnections; 
	for (auto it = connectionSet.begin(); it != connectionSet.end(); it++)
	{
		bool insert = true;
		for (size_t j = 0; j < toRemove.size(); j++)
		{
			if (toRemove[j] == *it)
			{
				insert = false; 
			}
		}
		if (insert)
		{
			newConnections.push_back(*it); 
		}
	} 

	successful = true;
 	return { newRoom, newWidth, newHeight, newConnections };
}


dungeon::dungeon(u8 numRoomsTarget, u8 biome, roomData& roomData)
{
	2 * 3;
	rooms.reserve(numRoomsTarget);
	bool isCompleted = true;
	int r = rand() % roomData.spawnRooms.size();
	u8 numRooms = 1;
	roomConstruct currentRoom = roomData.spawnRooms[r];
	r = rand() % roomData.roomTemplates.size();


	while (numRooms < numRoomsTarget)
	{
		if (currentRoom.connections.size() == 0)
		{
			break;
		}

		bool success = false;
		roomConstruct c = mergeRooms(currentRoom, roomData.roomTemplates[r], success);

		if (success)
		{
			numRooms++;
		}
		if (c.map.size() != 0)
		{
			currentRoom = c;
		}
		r = rand() % roomData.roomTemplates.size();
	}

	/*while (currentRoom.connections.size() > 0)
	{
		bool success = false;
		roomConstruct c = mergeRooms(currentRoom, mapManager::roomTemplates[r], success);

		if (c.map.size() != 0)
		{
			currentRoom = c;
		}
		r = mapManager::endCaps[rand() % mapManager::endCaps.size()];
	}*/
	u16 x = 0;
	u16 y = 0;
	rooms.emplace_back(room(currentRoom.map, currentRoom.width, currentRoom.height, &x, &y));
	playerSpawn = { x,y };
	tileTemplates = roomData.tileTemplates;

}

dungeon::dungeon(std::vector<std::vector<u8>> mapData, std::vector<u16> widths, std::vector<u16> heights)
{

	for (size_t i = 0; i < mapData.size(); i++)
	{
		rooms.push_back(room(mapData[i], widths[i], heights[i]));
	}
}

 
void room::iterate(std::vector<player>& players, std::vector<enemy>& enemies)
{
	for (size_t i = 0; i < objects.size(); i++)
	{
		switch (objects[i].type)
		{
		case walkOverPlayer:
			for (size_t j = 0; j < players.size(); j++)
			{
				if (false)//detectCollision(objects[i], players[j]))
				{ 
					objects[i].rotateTexVertices(1.57079633, 0);
					if (objects[i].func(&players[j]))
					{
						objects.erase(objects.begin() + i);
					}
				}
			}
			break;
		case activate:
			break;
		case walkOverEntity:
			break;
		}
	}
}

void room::addObject(vector2D<f32> vec, u8 type)
{
	objects.emplace_back(mapObject(vec, type));
}

size_t getMapSize(std::vector<u8>& vec)
{
	size_t numTiles = 0;
	for (size_t i = 0; i < vec.size(); i++)
	{
		if (vec[i] != 0)
		{
			numTiles++;
		}
	}
	return numTiles;
}
#define _USE_MATH_DEFINES
#include <math.h>
f32 degToRad(f32 in)
{
	return (M_PI / 180) * in;
}
f32 getTileRotation(std::vector<u8>& map, std::vector<tile> tileTemplates, size_t index)
{

	if (map[index ] == 255)
	{
		return 0.0f;
	}
	if (tileTemplates[map[index]].isWall)
	{
		if (map[index - 1] == 0)
		{
			return degToRad(-90);
		}
		if (map[index + 1] == 0)
		{
			return degToRad(90);
		}
		if (index < 17)
		{	
			return 0.0f;
		}
		if (map[index - 17] == 0)
		{
			return 0.0f;
		}
		if (index > 600)
		{
			return degToRad(180);
		}
		if (map[index + 17] == 0)
		{
			return degToRad(180);
		}
	}
	return 0.0f;
}


room::room(std::vector<u8> map, u16 width, u16 height, u16* spawnX, u16* spawnY) : sprite(getMapSize(map) * 4)
{
	this->width = width;
	this->height = height;
	this->tex = textureManager::getTexture("tiles");
	size_t vertArrayStart = 0;  

	for (size_t i = 0; i < map.size(); i++)
	{
		if (map[i] == 0) continue; 
		if (map[i] > 152 && map[i] < 255)
		{
			spawnPoints.push_back(vector2D<u16>{ static_cast<u16>(i % width), static_cast<u16>(i / width) });
			map[i] = map[i] - 152;
		}
		if (map[i] == 152)
		{
			*spawnX = static_cast<u16>(i % width);
			*spawnY = static_cast<u16>(i / width);
			map[i] = map[i] - 151;
		}
		setPosVertices(static_cast<f32>(i % width), static_cast<f32>(i / width), 1, 1, vertArrayStart);
		setTexVertices(static_cast<f32>(map[i] % tex->width) / (tex->width), static_cast<f32>(map[i] / tex->width) / tex->height, 1.0f / tex->width, 1.0f / tex->height, vertArrayStart);
		//rotateTexVertices(getTileRotation(map, i), vertArrayStart);

		vertArrayStart += 4;
	}
	this->map = map;
}


bool dungeon::isWall(vector2D<f32> in, f32 width, f32 height, size_t roomNum)
{
	vector2D<u16> pos = vector2D<u16>(vector2D<f32>{ in.x, in.y });
	if (tileTemplates[rooms[roomNum][pos]].isWall)
	{
		return true;
	}
	pos = vector2D<u16>(vector2D<f32>{ in.x + width, in.y });
	if (tileTemplates[rooms[roomNum][pos]].isWall)
	{
		return true;
	}
	pos = vector2D<u16>(vector2D<f32>{ in.x, in.y + height });
	if (tileTemplates[rooms[roomNum][pos]].isWall)
	{
		return true;
	}
	pos = vector2D<u16>(vector2D<f32>{ in.x + width, in.y + height });
	if (tileTemplates[rooms[roomNum][pos]].isWall)
	{
		return true;
	}
	/*for (size_t i = 0; i < doors.size(); i++)
	{
		if (doors[i].vertices[0].x < in.x && doors[i].vertices[2].y > in.x
				&& doors[i].vertices[0].y < in.y && doors[i].vertices[2].y)
		{ 
			return doors[i].isOpen();
		}
	}*/
	return false;
}
 



void interpretMapSheet(std::vector<roomConstruct>& destination, std::string file)
{
	for (size_t i = 0; i < file.size();)
	{

		std::vector<size_t> connections;
		std::vector<u8> data(2916, 0);
		u16 width = 0;
		u8 height = 0;
		size_t currentStart = 0;
		for (size_t j = i; true; j++)
		{
			if (file[j] == -1)
			{
				connections.push_back(j - i + width * height);
				continue;
			}
			if (file[j] == '#')
			{
				width = j - i;
				int f = j - i - width;
				data.insert(data.begin() + (width * height), file.begin() + i, file.begin() + j);
				i = j + 1;
				height++;
				continue;
			}
			if (file[j] == ';')
			{

				data.insert(data.begin() + (width * height), file.begin() + i, file.begin() + j);
				height++;
				destination.emplace_back(roomConstruct{ std::vector<u8>(data.begin(), data.begin() + width * height), width, height, connections }); 

				i = j + 1;
				currentStart = i + 1;
				break;
			}
			file[j] -= 0x30;
		}

	}
}
roomData::roomData()
{		
	tileTemplates.reserve(16);
	tileTemplates.emplace(tileTemplates.begin(), tile(1));


	tileTemplates.emplace(tileTemplates.begin() + 1, tile(0));
	tileTemplates.emplace(tileTemplates.begin() + 2, tile(1));
	tileTemplates.emplace(tileTemplates.begin() + 3, tile(1));
	tileTemplates.emplace(tileTemplates.begin() + 3, tile(1));

	for (size_t i = tileTemplates.size(); i < 256; i++)
	{

		tileTemplates.emplace(tileTemplates.begin() + i, tile(1));
	}

	interpretMapSheet(roomTemplates, dataFile("resources/dataFiles/mapTemplates.txt").data);
	interpretMapSheet(spawnRooms, std::string(spawnRoomsFile));
	
} 
struct node
{

	f32 fScore = INT_MAX;
	f32 gScore = 0;
	vector2D<u16> pos;
	node* cameFrom = nullptr;
	bool operator < (const node& other)
	{
		return fScore < other.fScore;
	}
};
struct KeyHasher

{
	std::size_t operator()(const vector2D<u16>& in) const
	{ 
		auto hash = in.x * 0x9e3779b97f4a7c15 ^ in.y;
		hash ^= hash >> 32;
		hash *= 0x4cd6944c5cc20b6d;
		hash ^= hash >> 29;
		hash *= 0xfc12c5b19d3259e9;
		hash ^= hash >> 32;
		return hash;
		
	}
};


 
#include <unordered_map>
#include "map.h"

f32 heuristicEstimate(vector2D<u16> pos, vector2D<u16> finish)
{
	return sqrt(pow(finish.x - pos.x, 2) + pow(finish.y - pos.y, 2));
}

std::vector<vector2D<u16>> dungeon::findPath(vector2D<u16> startPos, vector2D<u16> endPos, size_t currentRoom)
{ 
	std::unordered_map<vector2D<u16>, node*, KeyHasher> openSet;
	std::unordered_map<vector2D<u16>, node*, KeyHasher> closedSet;
	node* startNode = new node;
	vector2D<u16> closestPos = startPos;
	startNode->fScore = heuristicEstimate(startPos, endPos);
	startNode->gScore = 0;
	startNode->pos = startPos;
	openSet[startPos] = startNode;
	std::vector<vector2D<u16>> toReturn = { startPos };
	while (openSet.size() != 0)
	{
		auto findCurrent = openSet.find(closestPos); 
		if(findCurrent == openSet.end())
		{
			f32 lowestF = INT_MAX;
			for (auto it = openSet.begin(); it != openSet.end(); it++)
			{
				if (it->second->fScore < lowestF)
				{
					findCurrent = it;
					lowestF = it->second->fScore;
				}
			}
		} 

		node* current = findCurrent->second;
		if (current->pos == endPos)
		{ 
			toReturn.clear();
			toReturn.push_back(current->pos);
			while (current->cameFrom != nullptr)
			{
				current = current->cameFrom; 
				toReturn.push_back(current->pos);
			}
			break;
		}

		openSet.erase(openSet.find(current->pos));
		closedSet[current->pos] = current;

		std::vector<vector2D<u16>> neighbors;

		for (u16 y = current->pos.y - 1; y < current->pos.y + 2; y++)
		{
			for (u16 x = current->pos.x - 1; x < current->pos.x + 2; x++)
			{ 
				if (vector2D<u16>{x, y} != current->pos && y < rooms[currentRoom].height && x < rooms[currentRoom].width)
				{
					if (!tileTemplates[rooms[currentRoom][{x, y}]].isWall)
					{
						neighbors.push_back({ x,y });
					}
				}
			}
		}

		for (size_t i = 0; i < neighbors.size(); i++)
		{
			if (closedSet.find(neighbors[i]) != closedSet.end())
			{
				continue;
			}

			if (openSet.find(neighbors[i]) == openSet.end())
			{
				int gNeighbor = current->gScore + heuristicEstimate(current->pos, neighbors[i]);
				int fNeighbor = gNeighbor + heuristicEstimate(neighbors[i], endPos); 
				node* newNode = new node;
				newNode->fScore = heuristicEstimate(neighbors[i], endPos);
				newNode->gScore = 0;
				newNode->pos = neighbors[i];
				newNode->cameFrom = current;
				openSet[neighbors[i]] = newNode;

				if (newNode->fScore < current->fScore)
				{
					closestPos = newNode->pos;
				}
			}
		}
	}
	for (auto it = closedSet.begin(); it != closedSet.begin(); it++)
	{
		delete it->second;
	}
	for (auto it = openSet.begin(); it != openSet.begin(); it++)
	{
		delete it->second;
	}

	return toReturn;
}