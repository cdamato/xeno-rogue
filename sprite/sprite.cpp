#include "spriteImpl.h"   
#include "../png.h"
#include <math.h>

std::map<std::string, texture> textureManager::textures;


texture::texture(std::string str, u8 tw, u8 th)
{ 
	u32 textureID = 0;
	std::vector<unsigned char> data;
	unsigned int imgwidth;
	unsigned int imgheight;
	lodepng::decode(data, imgwidth, imgheight,
		str); 

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);



	//texture specification
	glTexImage2D
	(
		GL_TEXTURE_2D, 				
		0,		
		GL_RGBA,
		imgwidth,	
		imgheight,
		0,		
		GL_RGBA,
		GL_UNSIGNED_BYTE,	
		data.data()
	);
	 glGenerateMipmap(GL_TEXTURE_2D);

	 //texture clamping method
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	 //texture interpolation
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);

	buffer.buffer = textureID;
 
	width = tw;
	height = th; 
	
	f32 w = imgwidth / tw;
	f32 h = imgheight / th;
	f32 m = w > h ? w : h;
	w /= m;	h /= m;
	widthAspect = w;
	heightAspect = h;  
}

void textureManager::init()
{ 
	textures.emplace( "gun", texture("resources\\textures\\gun.png", 1, 1 ));
	textures.emplace("healthBar", texture("resources\\textures\\healthBar.png", 2, 1));
	textures.emplace("bullets",  texture("resources\\textures\\bullets.png", 1, 1));
	textures.emplace("tiles", texture("resources\\textures\\tiles.png", 5, 1));
	textures.emplace("mapObjects", texture("resources\\textures\\mapObjects.png", 2, 1));
	textures.emplace("playerLegs", texture("resources\\textures\\player\\playerLegs.png", 5, 8));
}

texture * textureManager::getTexture(const std::string & str)
{
	return &textures.at(str);
}