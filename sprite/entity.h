#ifndef ENTITY
#define ENTITY

#include "sprite.h"  

struct modifier
{
	int a;
	int b;
};

class weaponManager
{
	static std::vector<modifier> stocks;
	static std::vector<modifier> barrels;
};


enum AOEType
{
	bullet, 
	swing
};
typedef u8 weaponType;
typedef u8 entityType;  
 
class weapon;
class dungeon;
 
class projectile : public sprite<f32>
{
public:
	vector2D<f32> delta; 
	weapon* origin;
	timer t;
	projectile(vector2D<f32>, vector2D<f32>, vector2D<f32>, weapon*, weaponType );
	projectile(vector2D<f32>, vector2D<f32>, weapon*, weaponType );
	template <class T>
	bool moveprojectile(T& vec, dungeon*);
};


class controlPackageBase
{

};

template<typename T>
class entity;

class weapon
{
public:

	entity<controlPackageBase>* holder;
	std::vector<projectile> projectiles;
	ms cooldown = ms(500); 
	ms projectileLife = ms(1000); 
	u8 projectilesPerShot = 1;
	bool autofire;
	bool isReloading = false;
	u8 attackPower = 30;
	timer t;
	AOEType damageType = bullet;
	weaponType type;
	
	weapon(weaponType type, entity<controlPackageBase>* caller)
	{
		projectiles.reserve(32);
		this->type = type;
		this->holder = caller;
		t.start();
	}
	void fireprojectile(vector2D<f32>) ;
	template <class T>
	void iterateprojectiles(T& vec, dungeon* map)
	{
		for (size_t i = 0; i < projectiles.size(); i++)
		{
			bool collide = projectiles[i].moveprojectile(vec, map);
			if (collide || projectiles[i].t.getTimeElapsed<ms>() > projectileLife)
			{
				projectiles.erase(projectiles.begin() + i);
				i--;
			}
		}
	} 
};
 
 


template <typename T>
class entity;

 
template <typename T>
class entity : public multiSprite<animatedSprite<f32>>
{
public:
	u8 team;
	u8 health = 100;
	u8 defense = 20; 
	u8 quadrant = 0;
	std::vector<weapon> weapons;
	size_t currentWeapon = 0;
	T controlPackage;


	entity(vector2D<f32>, entityType type);
	entity(entity&& other);
	entity& operator= (entity&& other);

	void shoot(vector2D<f32> pos);
	void takeDamage(projectile& b); 
	void moveEntity(dungeon* map, std::vector<entity<T>>&);
}; 
 
class controlManagerPlayer : public controlPackageBase
{
public:
	struct 
	{
		bool leftButtonDown = false;
		bool rightButtonDown = false;
		bool moveLeft = false;
		bool moveRight = false;
		bool moveDown = false;
		bool moveUp = false;
		vector2D<u16> lastClick;
	} controlManager;
	controlManagerPlayer();
	sprite<u16> minimapIndicator;
	vector2D<f32> getFireDelta(vector2D<f32>);
	template<class T>
	vector2D<f32> getMoveDelta(vector2D<f32>, dungeon* map, std::vector<entity<T>>&);
};

class controlManagerEnemy : public controlPackageBase
{
public:
	u8 chaseRange = 10;
	u8 attackRange = 10;
	std::vector<vector2D<u16>> pathing;
	vector2D<f32> getFireDelta(vector2D<f32>);
	template<class T>
	vector2D<f32> getMoveDelta(vector2D<f32>, dungeon* map, std::vector<entity<T>>&);
};


using player = entity<controlManagerPlayer>;
using enemy = entity<controlManagerEnemy>;



#endif