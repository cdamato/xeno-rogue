#include "GLWM.h"
#include "globals.h"

u8 globals::currentTick;
u16 globals::fps;
f32 globals::speedModifier;

void keyboardCallback(int code, action action, windowState& state)
{ 
	 
	switch (code)
	{
	case VK_LEFT:
	case 'a':
	case 'A':
		state.players[state.playerID].controlPackage.controlManager.moveLeft = action;
		break;
	case VK_RIGHT:
	case 'd':
	case 'D':
		state.players[state.playerID].controlPackage.controlManager.moveRight = action;
		break;
	case VK_UP:
	case 'w':
	case 'W':
		state.players[state.playerID].controlPackage.controlManager.moveUp = action;
		break;
	case VK_DOWN:
	case 's':
	case 'S':
		state.players[state.playerID].controlPackage.controlManager.moveDown = action;
		break;

	case VK_OEM_PLUS: 
		state.camera[15] -= 50.0f / 120;
		break;
	case VK_OEM_MINUS: 
		state.camera[15] += 50.0f / 120;
		break;
	}
}

 

#ifdef _WIN32

vector2D<u16> getCursorPos(HWND handle)
{
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(handle, &p); 
	return { static_cast<u16>(p.x), static_cast<u16>(p.y) };
}

windowState& getState(HWND window)
{
	const auto user_data = GetWindowLongPtrW(window, GWLP_USERDATA);
	return *reinterpret_cast<windowState*>(user_data);
}

LRESULT CALLBACK WindowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	windowState& state = getState(handle);
	switch (message)
	{
	case WM_NCCREATE:
	{
		// Store the state pointer in user data now so we can access it later.
		const auto create_params = reinterpret_cast<const CREATESTRUCTW*>(lParam);
		SetWindowLongPtrW(handle, GWLP_USERDATA,
			reinterpret_cast<LONG_PTR>(create_params->lpCreateParams));
		return true;
	}
	case WM_MOUSEMOVE:
	{ 
		state.players[state.playerID].controlPackage.controlManager.lastClick = { static_cast<u16>(GET_X_LPARAM(lParam)), static_cast<u16>(GET_Y_LPARAM(lParam)) };
		return 0;
	}
	case WM_LBUTTONDOWN:
	{
		state.players[state.playerID].controlPackage.controlManager.leftButtonDown = true;
		state.players[state.playerID].controlPackage.controlManager.lastClick = getCursorPos(handle);
		return 0;
	}
	case WM_LBUTTONUP:
	{
		state.players[state.playerID].controlPackage.controlManager.leftButtonDown = false;
		return 0;
	}
	case WM_MOUSEWHEEL:
	{
		//state.camera[15] -= 50.0f / GET_WHEEL_DELTA_WPARAM(wParam); 
		return 0;
	}

	case WM_KEYDOWN:

		keyboardCallback(wParam, action::press, state);
		return 0;
	case WM_KEYUP:

		keyboardCallback(wParam, action::release, state);
		return 0;

		case WM_DESTROY:
	{
		std::unique_ptr<windowState> state_ptr(&getState(handle));
		PostQuitMessage(0);
		return 0;
	}
	}
	return DefWindowProc(handle, message, wParam, lParam);


}

 
void GLWMPollEvents(HWND handle)
{
	MSG msg = {};
	
	while (PeekMessage(&msg, handle, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);		
	}

}
extern "C" IMAGE_DOS_HEADER __ImageBase;
const HINSTANCE this_instance = reinterpret_cast<HINSTANCE>(&__ImageBase);

void initGlew()
{
	HWND hwnd = CreateWindowExW(
		0,                              // Optional window styles.
		L"game",                     // Window class
		L"game",    // Window text
		CS_OWNDC,            // Window style

							 // Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, 0, 0,

		NULL,       // Parent window    
		NULL,       // Menu
		NULL,  // Instance handle
		NULL        // Additional application data
	);
	HDC hdc = GetDC(hwnd);
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,
		8,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int  pf = ChoosePixelFormat(hdc, &pfd); //uhh this is just what windows gave me, probably not portable
	SetPixelFormat(hdc, pf, &pfd);

	HGLRC ourOpenGLRenderingContext = wglCreateContext(hdc);
	wglMakeCurrent(hdc, ourOpenGLRenderingContext);
	glewInit();
	wglMakeCurrent(hdc, 0);
	DestroyWindow(hwnd);
}

HWND initGLWindow( )
{
	WNDCLASSEXW wc = {};

	wc.cbSize = sizeof(wc);

	wc.lpfnWndProc = &WindowProc;
	wc.hInstance = this_instance;

	wc.lpszClassName = L"game";
	wc.hCursor = LoadCursorW(nullptr, IDC_ARROW);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	if (RegisterClassExW(&wc) == 0) {
		printf("whee");
	}
	RegisterClassExW(&wc);


	initGlew();

	


	u32 searchPos;
	u32 beginPos;
	u32 endPos;
	u16 windowWidth, windowHeight;
	dataFile file("resources/dataFiles/config.txt");

	searchPos = file.find("windowWidth", 0);
	beginPos = file.find("=", searchPos) + 1;
	endPos = file.find(";", searchPos);
	windowWidth = atoi(file.substring(beginPos, endPos - beginPos).c_str());
	printf("setting globalVal windowWidth to %i\n",  windowWidth);

	searchPos = file.find("windowHeight", 0);
	beginPos = file.find("=", searchPos) + 1;
	endPos = file.find(";", searchPos);
	windowHeight = atoi(file.substring(beginPos, endPos - beginPos).c_str());
	printf("setting globalVal windowHeight to %i\n", windowHeight);

	/*fps = 666;
	speedModifier = 60.0f / static_cast<f32>(fps);
	printf("\n");

	currentRoom = 0;
	currentTick = 0;*/

	auto state = std::make_unique<windowState>(windowWidth, windowHeight);
	state->width = windowWidth;
	state->height = windowHeight;
	state->camera = { 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };

	HWND hwnd = CreateWindowExW(
		0,                              // Optional window styles.
		L"game",                     // Window class
		L"game",    // Window text
		CS_OWNDC,            // Window style

							 // Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, windowWidth, windowHeight,

		NULL,       // Parent window    
		NULL,       // Menu
		NULL,  // Instance handle
		state.get()        // Additional application data
	);

	state.release();
	ShowWindow(hwnd, 1);
	return hwnd;


	//GLWMLoadFunctions();  
}

HDC getContext(HWND hwnd)
{
	HDC hdc = GetDC(hwnd);
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,
		8,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int  pf = ChoosePixelFormat(hdc, &pfd); //uhh this is just what windows gave me, probably not portable
	SetPixelFormat(hdc, pf, &pfd);

	HGLRC ourOpenGLRenderingContext = wglCreateContext(hdc);
	wglMakeCurrent(hdc, ourOpenGLRenderingContext);
	return hdc;
}


void GLWMSwapBuffers(HDC context)
{
	SwapBuffers(context);
}
 



#elif  __linux__
void GLWMPollEvents(GLWMWindow*)
{ 

}

// -- Written in C -- //

#include<stdio.h>
#include<stdlib.h>
#include<X11/X.h>
#include<X11/Xlib.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>
 

GLWMWindow* initGLWindow(const u16& width, const u16& height)
{
	Display                 *dpy;
	Window                  root;
	GLint                   att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
	XVisualInfo             *vi;
	Colormap                cmap;
	XSetWindowAttributes    swa;
	Window                  win;
	GLXContext              glc;  

 	dpy = XOpenDisplay(NULL);
 
 	if(dpy == NULL) {
        printf("\n\tcannot connect to X server\n\n");
        exit(0);
 	}
        
 	root = DefaultRootWindow(dpy);

 	vi = glXChooseVisual(dpy, 0, att);

 	if(vi == NULL) {
        printf("\n\tno appropriate visual found\n\n");
        exit(0);
 	} 
 	else {
        printf("\n\tvisual %p selected\n", (void *)vi->visualid); /* %p creates hexadecimal output like in glxinfo */
 	}


 cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);

 swa.colormap = cmap;
 swa.event_mask = ExposureMask | KeyPressMask;
 
 win = XCreateWindow(dpy, root, 0, 0, width, height, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &swa);

 XMapWindow(dpy, win);
 XStoreName(dpy, win, "VERY SIMPLE APPLICATION");
 
 glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
 glXMakeCurrent(dpy, win, glc);
 
 glEnable(GL_DEPTH_TEST); 
 
	return new GLWMWindow{&mouseClickCallback, &keyboardCallback, &scrollCallback, &cursorCallback, { dpy, win, &glc}};

}

vector2D<u16> getCursorPos(GLWMWindow* window)
{
  Window root_return, child_return;
  i32 root_x_return, root_y_return, win_x_return, win_y_return;
  u32 mask_return;
  XQueryPointer(window->impl.display, window->impl.window, &root_return, &child_return, &root_x_return, &root_y_return, 
                     &win_x_return, &win_y_return, &mask_return);
  return vector2D<u16>{static_cast<u16>(root_x_return), static_cast<u16>(root_y_return)};
}



void GLWMSwapBuffers(GLWMWindow* window)
{
  glXSwapBuffers(window->impl.display, window->impl.window);
}

#endif

