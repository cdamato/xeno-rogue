#ifndef SHADER 
#define SHADER
#include "GLWM.h"


struct shader
{
	GLuint program;
	GLint vertexAttribPos;
	GLint UVAttribPos; 
};

class shaderManager
{
public:
	shader spriteShader;
	shader UIShader; 
	shader minimapShader;
	shaderManager(f32 windowWidth, f32 windowHeight);
};
#endif