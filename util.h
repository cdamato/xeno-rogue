#ifndef UTIL
#define UTIL

#include <string>
#include <map>
#include <set>
#include <vector>
#include <fstream> 
#include <chrono>

typedef uint8_t u8;
typedef int8_t i8;
typedef uint16_t u16;
typedef int16_t i16;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef int64_t i64;
typedef float f32;
typedef double f64;
 
enum mouse
{
	leftButton,
	rightButton,
	middleButton
};
#define MAX(a,b) ((a) > (b) ? (a) : (b))
enum action
{
	release,
	press,
};
 
template<class T>
struct vector2D
{
	T x, y;
	bool operator < (const vector2D& other) const
	{
		if (other.x != x)
			return x < other.x;
		return y < other.y;
	}
	bool operator == (const vector2D& other) const
	{
		return (x == other.x && y == other.y);
	}
	bool operator != (const vector2D& other) const
	{
		return !operator==(other);
	}
	const vector2D operator - (const vector2D & other) const
	{
		return vector2D({ x - other.x, y - other.y });
	}
	const vector2D operator + (const vector2D & other) const
	{
		return vector2D({ x + other.x, y + other.y });
	}
	const vector2D& operator += (const vector2D& other)
	{
		*this = *this + other;
		return *this;
	}
	const vector2D& operator -= (const vector2D& other)
	{
		*this = *this - other;
		return *this;
	}
	template<typename T2>
	explicit operator vector2D<T2>()
	{
		return vector2D<T2>{ static_cast<T2>(x), static_cast<T2>(y) };
	}
};
 


template <class t>
float pythagorean(t a, t b) 
{
	return (float)sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2));
}
 
typedef std::chrono::milliseconds ms;
typedef std::chrono::nanoseconds ns;
typedef std::chrono::seconds s; 

class timer
{	
	std::chrono::steady_clock::time_point startPoint; 
public:
	void start(); 
	template <class t>
	t getTimeElapsed();
};

template<class t>
inline t timer::getTimeElapsed()
{
	return std::chrono::duration_cast<t>(std::chrono::steady_clock::now() - startPoint);
}

inline void timer::start()
{
	startPoint = std::chrono::steady_clock::now();
}






class dataFile 
{
public:
	std::string data;
	int numEntries; 

	dataFile(const std::string&);
	int find(const std::string&, const int&);
	std::string substring(const int&, const int&);
	void erase(const int&, const int&);
};

inline dataFile::dataFile(const std::string& filename)
{

	short numOpenParen = 0;
	short numCloseParen = 0;
	short numSemiColon = 0;

	std::ifstream inputFile(filename);
	this->data = std::string((std::istreambuf_iterator<char>(inputFile)),
		std::istreambuf_iterator<char>());

	inputFile.close();


	for (int i = 0; i < data.size(); i++)
	{
		switch (data[i])
		{
		case 13:
		case 10: 
		case 9:
		case 32:
			data.erase(i, 1);
			i--;
			break;
		case '{':
			numOpenParen++;
			break;
		case '}':
			numCloseParen++;
			break;
		case ';':
			numSemiColon++;
			break;
		} 
	}

	std::string fileType = filename.substr(filename.length() - 3, 3);
	if (fileType == "txt") numCloseParen == numOpenParen ? numEntries = numOpenParen : numEntries = 0;
	if (fileType == "csv") numEntries = numSemiColon;

	printf("Datafile %s imported properly. Number of entries is %i.\n", filename.c_str(), numEntries);
	printf("%s\n", data.c_str());
}

inline int dataFile::find(const std::string& search, const int& startPos)
{
	if (data == "") return -1;
	return (int) data.find(search, startPos);
}

inline std::string dataFile::substring(const int& begin, const int& end)
{
	if (data == "") return "";
	int endPos = end - begin;
	if (end == -1) endPos = -1;
	return data.substr(begin, endPos);
}

inline void dataFile::erase(const int& begin, const int& end)
{
	if (data == "") return;

	int endPos = end - begin;
	if (end == -1) endPos = -1;
	data.erase(begin, endPos);
}

 


#endif