#include "shader.h" 
#include "GLWM.h"
 
void genShader(u32& shader, const char* code)
{
	glShaderSource(shader, 1, &code, 0);
	glCompileShader(shader);
}

void checkShader(u32 shaderID)
{ 
	i32 Result = GL_FALSE;
	int InfoLogLength;

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) { 
		std::vector<char>shaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &shaderErrorMessage[0]);
		printf("%s\n", &shaderErrorMessage[0]);
	}
}

shader LoadShader(const char * vertexShaderCode, const char * fragmentShaderCode) {

	// Create the shaders
	u32 vertexShader = glCreateShader(GL_VERTEX_SHADER);
	u32 fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Compile Vertex Shader
	genShader(vertexShader, vertexShaderCode);
	checkShader(vertexShader);
	// Compile Fragment Shader
	genShader(fragmentShader, fragmentShaderCode);
	checkShader(fragmentShader);
	// Link the program
	u32 shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// Cleanup
	glDetachShader(shaderProgram, vertexShader);
	glDetachShader(shaderProgram, fragmentShader);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);


	return shader{ shaderProgram, glGetAttribLocation(shaderProgram, "vertexPos"),  glGetAttribLocation(shaderProgram, "vertexUV")};

}

shaderManager::shaderManager(f32 windowWidth, f32 windowHeight)
{
	//simplify this, somehow
	f32 halfWidth =  (128.0f /windowWidth) ;
	f32 halfHeight = (128.0f / windowHeight) ;

	const char* fragShader =
		"#version 120\n"

		"uniform sampler2D tex;" 
		"varying vec2 UV;"

		"void main()"
		"{" 
		"gl_FragColor = vec4(texture2D(tex, UV).rgba);"
		"}";
	const char* vertShader =
		"#version 120\n"

		"uniform vec2 half_viewport_size;" 
		"uniform mat4 viewMatrix;"

		"attribute vec3 vertexPos;"
		"attribute vec2 vertexUV;"

		"varying vec2 UV;"

		"void main()"
		"{"

		"gl_Position =  viewMatrix * vec4(vertexPos.x  * half_viewport_size.x- 1.0, -vertexPos.y * half_viewport_size.y + 1.0,  1,  1);"


		"UV = vertexUV;"
		"}"; 
	const char* UIVertShader =
		"#version 120\n"

		"uniform vec2 half_viewport_size;" 

		"attribute vec3 vertexPos;"
		"attribute vec2 vertexUV;"

		"varying vec2 UV;"

		"void main()"
		"{"
		
		"gl_Position =  vec4(vertexPos.x * half_viewport_size.x - 1.0, vertexPos.y  * -half_viewport_size.y + 1.0,  1,  1);"


		"UV = vertexUV;"
		"}"; 
	const char* minimapVertShader =
		"#version 120\n"

		"uniform vec2 half_viewport_size;"
		"uniform vec2 pos;" 

		"attribute vec3 vertexPos;"
		"attribute vec2 vertexUV;"

		"varying vec2 UV;"

		"void main()"
		"{"

		"gl_Position =  vec4((vertexPos.x + pos.x) * half_viewport_size.x - 1.0, (vertexPos.y + pos.y) * -half_viewport_size.y + 1.0,  1,  1);"


		"UV = vertexUV;"
		"}";

	spriteShader = LoadShader(vertShader, fragShader);
	UIShader = LoadShader(UIVertShader, fragShader); 
	minimapShader = LoadShader(minimapVertShader, fragShader);

	glUseProgram(spriteShader.program); 
	glUniform2f(glGetUniformLocation(spriteShader.program, "half_viewport_size"), halfWidth, halfHeight); 
	glUseProgram(UIShader.program); 
	glUniform2f(glGetUniformLocation(UIShader.program, "half_viewport_size"), 2.0f / windowWidth, 2.0f / windowHeight);
	glUseProgram(minimapShader.program);
	glUniform2f(glGetUniformLocation(minimapShader.program, "half_viewport_size"), 2.0f / windowWidth, 2.0f / windowHeight);

	glUniform2f(glGetUniformLocation(minimapShader.program, "pos"), 0, 0);

}
