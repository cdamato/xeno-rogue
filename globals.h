#ifndef GLOBALS
#define GLOBALS

#include "sprite/map.h" 
#include "shader.h"
#include "GLWM.h"
class globals
{
public:
	static u16 fps;
	static u8 currentTick;
	static f32 speedModifier;
};

struct windowState
{
	windowState(u16 w, u16 h) : shaders(w, h)
	{
		width = w;
		height = h; 
		camera = { 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };
	}
	u16 width, height;
	graphicsContext context; 
	shaderManager shaders;
	std::vector<f32> camera; 
	dungeon* map;
	size_t playerID;
	std::vector<std::vector<enemy>> enemies;
	std::vector<player> players;
};
#endif  
