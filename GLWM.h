#ifndef GLWM
#define GLWM


#ifndef UNICODE
#define UNICODE
#endif 
#include "util.h" 
#include "glew.h"  
 

#ifdef _WIN32


#include <Windows.h>
#include <Windowsx.h> 



#define WINDOWCLOSE WM_DESTROY
#define LBUTTONPRESS WM_LBUTTONDOWN
#define LBUTTONRELEASE WM_LBUTTONUP
#define RBUTTONPRESS WM_RBUTTONDOWN
#define RBUTTONRELEASE WM_RBUTTONUP
typedef HDC graphicsContext;
typedef HWND windowHandle;

class windowState;
vector2D<u16> getCursorPos(HWND);
HWND initGLWindow();
HDC getContext(HWND);
void GLWMPollEvents(HWND);
void GLWMSwapBuffers( HDC); 
windowState& getState(HWND);
#elif __linux__
#include <X11/Xlib.h> 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/gl.h>
#include <GL/glx.h>
 
struct windowImpl //X11 
{  
    Display* display;
    Window window;
    GLXContext * context;
};
#endif
 
 
#endif