#ifndef NETWORK
#define NETWORK
#define _SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING
#define ASIO_STANDALONE 1 
#include "asio.hpp"
#include <iostream> 
typedef asio::mutable_buffers_1 buffer;
typedef asio::ip::tcp::socket networkSocket;
std::string readInfo(networkSocket& socket, size_t& bufferSize)
{ 
	size_t len = 0;
	bool isCompleted = false;
	asio::error_code error; 
	std::stringstream ss;
	 
	asio::streambuf sb; 
	std::string s;

	bufferSize = asio::read_until(socket, sb, ";\n");
	std::istream is(&sb); 
	is >> s;
	ss << s; 
	
	while (s[bufferSize - 2] != ';')
	{
		bufferSize = asio::read_until(socket, sb, ";\n");
		std::istream is(&sb);
		is >> s;
		ss << s;
	}
	//what's the better way to do this?

	std::string toReturn;
	ss >> toReturn;

	printf("message = "); 
	std::cout << toReturn; 
	printf("\n");
	bufferSize = len;


	return s;
}
template<typename T>
void sendInfo(networkSocket& socket, T message)
{
	asio::error_code ignored_error;

	asio::write(socket, asio::buffer(message), ignored_error);
	printf("Message: =");
	std::cout.write(message.data(), message.size());
	printf("\n");
}
#endif